import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  id = 'chart1';
  id2 = 'chart2';
  id3 = 'chart3';
  width = 600;
  height = 300;
  type = 'column2d';
  dataFormat = 'json';
  dataSource;
  dataSource2;
  dataSource3;
  title = 'Bar Chart';
  type2 = 'pie3d';
  type3 = 'line';


  constructor() {
    this.dataSource = {
      "chart": {
        "caption": "Harry's SuperMart",
        "subCaption": "Top 5 stores in last month by revenue",
        "numberprefix": "$",
        "theme": "fint"
      },
      "data": [
        {
          "label": "Bakersfield Central",
          "value": "880000",
          "tooltext": "You are hovering"
        },
        {
          "label": "Garden Groove harbour",
          "value": "730000",
          "tooltext": "You are hovering"

        },
        {
          "label": "Los Angeles Topanga",
          "value": "590000",
          "tooltext": "You are hovering"

        },
        {
          "label": "Compton-Rancho Dom",
          "value": "520000",
          "tooltext": "You are hovering"

        },
        {
          "label": "Daly City Serramonte",
          "value": "330000",
          "tooltext": "You are hovering"

        }
      ]
    };

    this.dataSource2 = {
      "chart": {
        "caption": "Age profile in 3D pie Chart",
        "subcaption": "Last Year",
        "startingangle": "120",
        "showlabels": "0",
        "showlegend": "1",
        "enablemultislicing": "0",
        "slicingdistance": "15",
        "showpercentvalues": "1",
        "showpercentintooltip": "0",
        "plottooltext": "Age group : $label Total visit : $datavalue",
        "theme": "ocean"
      },
      "data": [
        {
          "label": "Teenage",
          "value": "1250400",

        },
        {
          "label": "Adult",
          "value": "1463300"
        },
        {
          "label": "Mid-age",
          "value": "1050700"
        },
        {
          "label": "Senior",
          "value": "491000"
        }
      ]
    };
    this.dataSource3 = {
      "chart": {
        "captio1n": "Bakersfield Central - Total footfalls",
        "subCaption": "Last week",
        "xAxisName": "Day",
        "yAxisName": "No. of Visitors (In 1000s)",
        "showValues": "0",
        "theme": "ocean",
        "borderColor": "#666666",
        "borderThickness": "4",
        "borderAlpha": "80",
        "bgColor": "#DDDDDD",
        "bgAlpha": "50"
      },

      "data": [
        {
          "label": "Mon",
          "value": "15123"
        },
        {
          "label": "Tue",
          "value": "14233"
        },
        {
          "label": "Wed",
          "value": "25507"
        },
        {
          "label": "Thu",
          "value": "9110"
        },
        {
          "label": "Fri",
          "value": "15529"
        },
        {
          "label": "Sat",
          "value": "20803"
        },
        {
          "label": "Sun",
          "value": "19202"
        }
      ]
    };
  }

}
